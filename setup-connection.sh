#!/bin/bash
rm /etc/NetworkManager/system-connections/*
cp network-manager/* /etc/NetworkManager/system-connections/
chmod 600 /etc/NetworkManager/system-connections/*
systemctl restart NetworkManager
nmcli con up id USB\ NET
