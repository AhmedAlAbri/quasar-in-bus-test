import serial 
import os
import boto3
import sys
import time
import datetime
import subprocess
import threading
#connect to AWS S3
print ('open s3')
s3 = boto3.resource('s3', 
        aws_access_key_id='AKIA55GYUTVFR2PH75PV',
        aws_secret_access_key='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'
)

cam_url="rtsp://eMushrifCam:2au8mGMzxD@192.168.224.88:554/11"
mybucket = 'em-sandbox-scratch'
mylocalpath = ''

def capture_img(): 
        imgname = datetime.datetime.now().strftime('Quasar-%Y-%m-%d_%H-%M-%S') + '.jpg'
        subprocess.run(['ffmpeg', '-i', cam_url, '-f', 'image2', '-frames', '1', '-update', '1', '-vf', 'scale=640:-1', imgname])
        return imgname


def upload_img(imagename):
        print(imagename)
        s3.meta.client.upload_file(mylocalpath+imagename, mybucket, "images/"+imagename)
        os.remove(imagename)

def read_serial_loop():
        with serial.Serial('/dev/ttymxc2', 115200, timeout=1) as ser:
                while 1:
                        #x = ser.read() # read one byte ...  
                        #s = ser.read(10) # read up to ten bytes (timeout)
                        line = ser.readline()   # read a '\n' terminated line
                        if line == b'': continue
                        cmd = line.decode('ascii').strip()
                        #print (line.decode('ascii').strip())
                        print (cmd)
                        if cmd =="Capture":
                                print ("capturing ...")
                                imgname = capture_img()
                                print ("capturing finished.")
                                print ('Uploading to S3')
                                upload_img(imgname)
                                print ('Image uploaded')
                        elif cmd == "Record":
                                print ("Start Recording ...")
                        elif cmd == "StopRecording":
                                print ("Stop Recording")
                        else:
                                print ("Invalid Command")

def capture_loop():
        while True:
                n = capture_img()
                upload_img(n)
                time.sleep(60 * 1) 

print("Sleep for 2 seconds")
time.sleep(2)
subprocess.run(['nmcli', 'connection', 'up', 'id', 'USB NET'])

thd1 = threading.Thread(target=capture_loop)
thd2 = threading.Thread(target=read_serial_loop)

print ('start thd')
thd1.start()
thd2.start()

print ('join thd')
thd1.join()
thd2.join()

print('Stopping...')
