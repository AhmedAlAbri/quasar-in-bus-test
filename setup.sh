#!/bin/bash
apt update
apt install python3-venv -y
mkdir -p /opt/quasar
cp quasar-capture-service.py /opt/quasar/quasar-capture-service.py
cp quasar-capture-aws.service /etc/systemd/system/quasar-capture-aws.service
cp requirements.txt /opt/quasar/requirements.txt
cd /opt/quasar
python3 -m venv env
env/bin/python -m pip install -r requirements.txt
systemctl daemon-reload
systemctl enable quasar-capture-aws.service
systemctl start quasar-capture-aws.service
